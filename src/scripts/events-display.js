document.addEventListener("DOMContentLoaded", async () => {
    const eventsContainer = document.getElementById("events_row");
    const wydarzeniaSection = document.getElementById("wydarzenia");

    try {
        const response = await fetch("data/events.json");
        const events = await response.json();

        if (events.length > 0) {
            eventsContainer.style.display = "flex"; // Show the container if events exist

            events.sort((a, b) => new Date(a.scheduled_start_time) - new Date(b.scheduled_start_time));

            events.forEach(event => {
                const eventCard = document.createElement("div");
                eventCard.classList.add("event_card");
                // TODO: change this event to toggle the expand, not when you press anthithing
                eventCard.setAttribute("onclick", "toggleDescription(this)");

                // Construct the image URL
                const imageUrl = event.image
                    ? `https://cdn.discordapp.com/guild-events/${event.id}/${event.image}.png?size=2048`
                    : null;

                // Format date and time to Polish locale
                const eventDate = new Date(event.scheduled_start_time);
                const formattedDate = eventDate.toLocaleDateString("pl-PL", { day: 'numeric', month: 'long', year: 'numeric' });
                const formattedTime = eventDate.toLocaleTimeString("pl-PL", { hour: '2-digit', minute: '2-digit' });

                // Convert URLs in the description to clickable links
                const descriptionWithLinks = event.description.replace(
                    /(https?:\/\/[^\s]+)/g,
                    '<a href="$1" class="link_clickable link_breakable" target="_blank">$1</a>'
                );

                // Determine location or channel link
                let locationContent = "N/A";
                if (event.entity_metadata.location) {
                    locationContent = event.entity_metadata.location;
                } else if (event.channel_id) {
                    locationContent = `<a class="link_clickable" href="https://discord.com/channels/1300527999157801032/${event.channel_id}" target="_blank">Join Channel</a>`;
                }

                eventCard.innerHTML = `
                    ${imageUrl ? `<div class="event_image_container"><img src="${imageUrl}" alt="${event.name}" class="event_image"></div>` : ''}
                    <h3>${event.name}</h3>
                    <p class="event_description">${descriptionWithLinks}</p>
                    <p class="location"><span class="polygon-color-text">Miejsce: </span>${locationContent}</p>
                    <p class="start_time "><span class="polygon-color-text">Godzina: </span>${formattedTime}</p>
                    <p class="start_time"><span class="polygon-color-text">Data: </span>${formattedDate}</p>
                    <p class="link_clickable link_expand">Zobacz więcej</p>
                `;

                eventsContainer.appendChild(eventCard);
            });
        } else {
            wydarzeniaSection.style.display = "none"; // Hide the entire section if no events
        }
    } catch (error) {
        console.error("Error loading events:", error);
        wydarzeniaSection.style.display = "none"; // Hide the entire section in case of an error
    }
});

// Toggle function for expanding/collapsing description
function toggleDescription(card) {
    const description = card.querySelector(".event_description");
    const expandLink = card.querySelector(".link_expand");

    description.classList.toggle('expanded');

    // Change the link text based on the expanded state
    if (description.classList.contains('expanded')) {
        expandLink.textContent = 'Zobacz mniej';
    } else {
        expandLink.textContent = 'Zobacz więcej';
    }
}
